$(document).ready(function() {

  $('.main-menu li.drop-level')
		.on('mouseover', function(){       
      $('.main-menu .main-menu-li a').addClass('over');
      $(this).find('.nav-secondary').removeClass('hidden');  
      $('.nav-secondary a').removeClass('over');
		})
		.on('mouseout', function(){
			$(this).find('.nav-secondary').addClass('hidden');
      $('.main-menu .main-menu-li a').removeClass('over');
	});

  $('.accordion-toggle').on('click', function() {
    $(this).find('i').toggleClass('glyphicon glyphicon-minus');
  });
});